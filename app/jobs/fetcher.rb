require 'net/http'
require 'json'
require 'active_record'
require 'faraday'
require './app/models/temp'
class Fetcher
  @queue = "main"
  def self.perform 
    url  = "https://dataservice.accuweather.com/currentconditions/v1/#{ENV['APILOC']}/historical/24?apikey=#{ENV['APIKEY']}"
    response = Faraday.get(url).body
    jsn = JSON.parse(response)
    for value in jsn do
        dt = Time.at(value["EpochTime"]).to_datetime rescue next; 
        STDOUT.puts "#{dt} #{value["Temperature"]["Metric"]["Value"]}"
        Temp.create({created_at: dt, val: value["Temperature"]["Metric"]["Value"] })
    end
    "Finish collect data job"
  end
end
