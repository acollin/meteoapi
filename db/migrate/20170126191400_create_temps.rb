class CreateTemps < ActiveRecord::Migration[6.0]
  def change
    create_table :temps do |t|
      t.float :val
      
      t.timestamps
    end
    add_index :temps, :created_at, unique: true, name: 'index_created_at'
  end
end

