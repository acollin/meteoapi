require File.expand_path('../config/environment', __FILE__)
require 'resque/server'
require 'resque/scheduler/server'

use Rack::ShowExceptions
use Rack::ConditionalGet
use Rack::ETag


require 'rack/cors'
use Rack::Cors do
  allow do
    origins '*'
    resource '*', headers: :any, methods: [ :get, :post, :put, :delete, :options ]
  end
end

Resque.redis = Redis.new(:host =>  ENV['REDIS_HOST'], :port =>  ENV['REDIS_PORT'])
Resque.logger.level = Logger::DEBUG

run Rack::URLMap.new \
  "/"       => API::Base, 
  "/resque" => Resque::Server.new

File.expand_path('./config/environment', __FILE__)

