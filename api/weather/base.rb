module Weather
  class Base < Grape::API

    helpers do
      def return_no_content_status
        status :no_content
        {}
      end
    end

    require File.dirname(__FILE__) + '/root'
    mount Weather::Root

  end
end

