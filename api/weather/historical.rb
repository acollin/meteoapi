module Weather
  class Historical < Grape::API
    rescue_from NotFoundError, ActiveRecord::RecordNotFound do |exception|
      Log.instance.log_request(:warn, env, exception)
      error!('Product not found', 404)
    end

    rescue_from ActiveRecord::RecordInvalid do |exception|
      Log.instance.log_request(:warn, env, exception)
      error!(exception.message, 409)
    end

    desc 'Return weather metrics per hour for 24h.'
    get :historical do
       # Почасовая температура за последние 24 часа
      temps = Temp.latest.for24hours.all()
      raise NotFoundError if temps.nil?
      present temps, with: API::Entities::Temp
    end

    resource :historical do
        desc 'Return maximal temperature for 24 hour.'
        get :avg do
          temp = Temp.latest.for24hours.average(:val)
          raise NotFoundError if temp.nil?
          { "avg": temp }
        end

        desc 'Return minimal temperature for 24 hour.' 
        get :min do
          temp = Temp.latest.for24hours.minimum(:val)
          raise NotFoundError if temp.nil?
          { "min": temp }
        end

        desc 'Return average temperature for 24 hour.'
        get :max do
          temp = Temp.latest.for24hours.maximum(:val)
          raise NotFoundError if temp.nil?
          { "max": temp }
        end
    end
  end
end

