# MeteoApi

## Dependencies

- Rack - Main APP
- ActiveRecord - Dep for postgress conn
- require_all for dependencies loading
- Dotenv for env's settings
- Ruby 2.6.3
- Postgres as main storage
- Docker - Deployment
- Grape as REST API
- VCR for integrate testing
- Faraday - For external requests
- Resque sheduler - for queue background tasks
- Resque - Worker for run tasks in queue
- Trailblazer - Not using
- Swagger Grape - for generate grape endpoints docs as JSON
- SwaggerUI - Panel for exploring api endpoints
- minitest, rspec, rack-test - Unit testing
- simplecov - Coverage
- factory_bot, database_cleaner - Fixtures for tests and data cleaner
  
## Installation

- Clone poject

To run application on docker:

- Install Docker and Docker-Compose
- Clone the project
- Run these commands on project root:

```shell
$ docker-compose build
$ docker-compose up

# Open another terminal and run:
$ docker-compose run api bundle exec rake db:create db:migrate
```

- Set API key to env file for accuweather.com (myself has been banned%))
- Do restarting the docker:

```shell
docker-compose down
docker-compose up

```

## Console

To use console, run the following command:

```shell
$ bin/console
```

## Tests

To execute tests, run the following command:

```shell
$ bundle exec rspec
```

## Routes

To show the application routes, run the following command:

```shell
$ bundle exec rake routes
```

## Running background tasks and panel

Panel below contains all the registered queues with the number of jobs currently in the queue. Select a queue from above to view all jobs currently pending on the queue.

```shell
http://localhost:3000/resque/
```

## Swagger and API Documentation

Running as dedicated service and fully synchronized with grape API
To access swagger documentation, enter the root application address in the browser:

```shell
http://localhost:8080/
```