
ENV["NO_DB"] = "true"
require "faraday"

describe 'Check gets data from accuweather' do
    it 'returns data from service as json' do
        VCR.use_cassette("accuweather") do
            url = "https://dataservice.accuweather.com/currentconditions/v1/#{ENV['APILOC']}/historical/24?apikey=#{ENV['APIKEY']}"
            data = Faraday.get(url).body
            # response = Net::HTTP.get_response(URI("https://dataservice.accuweather.com/currentconditions/v1/#{ENV['APILOC']}/historical/24?apikey=#{ENV['APIKEY']}"))
            jsonBody = JSON.parse(data, symbolize_names: true)
            expect(jsonBody[0]).to have_key(:EpochTime)
        end
    end
end