FactoryBot.define do
  factory :temp do
    val { 24.4 }
    created_at { Time.now } 
  end
end

